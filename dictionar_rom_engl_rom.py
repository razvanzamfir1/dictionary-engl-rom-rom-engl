cuvinte_romana = ['mama', 'tata']
cuvinte_engleza = ['mother', 'father']
while True:  # se pune pentru a ca programul sa ruleze in continuu pana ii dam comanda de oprire
    rom_eng = input('Apasati 1 pentr rom-eng si 2 pentru eng-rom: ')  # se alege pe ce dict se va duce meniul
    while rom_eng != '1' and rom_eng !='2':  # punem 1 si 2 in ghilimele pentru ca nu putem compara un str cu un int
        print('Ati introdus alte caractere!')  # de ex daca introducem '3'
        rom_eng = input('Apasati 1 pentr rom-eng si 2 pentru eng-rom: ')  # se repeta bucla

    if rom_eng == '1':  # daca am intrat pe ramura 1
        cuvant = input('Introduceti cuvantul in romana: ')
        # in acest if se face legatura intre cuvantul in Ro si cel in engleza prin indx. Are loc traducerea
        if cuvant in cuvinte_romana:
            indx = cuvinte_romana.index(cuvant) #se obtine indexul cuvantului in romana
            print(cuvinte_engleza[indx])
               # daca nu are loc traducerea intram pe else
        else:
            raspuns = input('Nu exista in baza de date, sti traducerea (Yes/No)?')
            if raspuns == 'yes':    # trebuie introdusa traducerea de utilizator manual
                traducere = input('Scrieti traducerea: ')
                cuvinte_romana.append(cuvant)   # se adauga la finalul listei rom
                cuvinte_engleza.append(traducere)   # se adauga la finalul listei engl
            else:
                print('Nu exista traducere!')

    else:
        word = input('Introduceti cuvantul in engleza: ')
        # in acest if se face legatura intre cuvantul in Ro si cel in engleza prin indx. Are loc traducerea
        if word in cuvinte_engleza:
            indx = cuvinte_engleza.index(word)  # se obtine indexul cuvantului in romana
            print(cuvinte_romana[indx])
            # daca nu are loc traducerea intram pe else
        else:
            raspuns = input('Nu exista in baza de date, sti traducerea (Yes/No)?')
            if raspuns == 'yes':  # trebuie introdusa traducerea de utilizator manual password-manager
                traducere = input('Scrieti traducerea: ')
                cuvinte_engleza.append(word)  # se adauga la finalul listei rom
                cuvinte_romana.append(traducere)  # se adauga la finalul listei engl
            else:
                print('Nu exista traducere!')

                #trebuie implementata inserarea intr-o baza de date a cuvintelor introduse de la tastatura.
                #momentan ele nu se salveaza la orpirea scriptului.





